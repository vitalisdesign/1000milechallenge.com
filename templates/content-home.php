<?php 
use Roots\Sage\Assets;

$signup_menu_item = get_page_by_path('sign-up', 'OBJECT', 'nav_menu_item');
$signup_url = get_post_meta($signup_menu_item->ID, '_menu_item_url', true);
?>

<div class="header__spacer"></div>

<div class="home__banner" style="background-image: url(<?= get_field('banner_image')['sizes']['max']; ?>);">
    <div class="home__banner__headline">
        <div class="home__banner__headline__emphasis"><?= get_field('banner_headline_emphasis'); ?></div>
        <div class="home__banner__headline__copy"><?= get_field('banner_headline_copy'); ?></div>
    </div>

    <div class="home__banner__cta">
        <div class="home__banner__cta__heading"><?= get_field('banner_cta_heading'); ?></div>
        <div class="home__banner__cta__copy"><?= get_field('banner_cta_copy'); ?></div>

        <a class="home__banner__cta__button ui-button ui-button--secondary" href="<?= $signup_url; ?>" style="background-color: <?= get_field('banner_button_color'); ?>"><?= get_field('banner_button_text'); ?></a>
    </div>
</div>

<div class="home__community home__section">
    <div class="container">
        <h2 class="home__section__heading">Welcome to<br /> the Community!</h2>

        <div class="home__community__inner-container">
            <div class="home__community__testimonials">
                <?php 
                if (have_rows('testimonials')) :
                    // display three testimonials at random
                    $testimonials = get_field_object('testimonials')['value'];
                    $random_keys = array_rand($testimonials, 3);

                    while (have_rows('testimonials')) : the_row();
                        if (in_array(get_row_index() - 1, $random_keys)) {
                            ?>
                            <div class="home__community__testimonials__item">
                                <div class="home__community__testimonials__image" style="background-image: url(<?= get_sub_field('image')['sizes']['medium']; ?>);"></div>

                                <div class="home__community__testimonials__info">
                                    <?= get_sub_field('name') . ', ' . get_sub_field('age'); ?><br />
                                    <?= get_sub_field('city') . ', ' . get_sub_field('country'); ?>
                                </div>

                                <div class="home__community__testimonials__quote">"<?= get_sub_field('quote'); ?>"</div>
                            </div>
                            <?php
                        }
                    endwhile;
                endif;
                ?>
            </div>
        </div>
    </div>
</div>

<div class="home__how-it-works home__section">
    <div class="container">
        <h2 class="home__section__heading">How It Works</h2>

        <div class="home__how-it-works__list">
            <ul class="home__how-it-works__nav">
                <?php
                for($i = 1; $i<5; $i++) {
                    ?>
                    <li><a id="nav-panel-<?= $i; ?>" href="#panel-<?= $i; ?>"></a></li>
                    <?php
                }
                ?>
            </ul>

            <?php 
            if (have_rows('how_it_works_panels')) :
                while (have_rows('how_it_works_panels')) : the_row();
                    $i = get_row_index();

                    if ($i == 1) {
                        $activeClass = 'active';
                    } else {
                        $activeClass = '';
                    }
                    ?>
                    <div class="home__how-it-works__item-<?= $i; ?> home__how-it-works__item <?= $activeClass; ?>" id="panel-<?= $i; ?>">
                        <div class="home__how-it-works__text">
                            <div class="home__how-it-works__heading">
                                <div class="home__how-it-works__heading__number"><?= $i; ?>.</div>
                                <h3 class="home__how-it-works__heading__text"><?= get_sub_field('heading'); ?></h3>
                            </div>

                            <div class="home__how-it-works__copy"><?= get_sub_field('copy'); ?></div>
                        </div>

                        <?php 
                        if ($i != 3) { 
                            $mobile_image_size = getimagesize(Assets\asset_path('images/how-it-works/mobile/how_it_works_mobile__' . $i . '@2x.png'));
                            ?>
                            <img class="home__how-it-works__image-mobile" src="<?= Assets\asset_path('images/how-it-works/mobile/how_it_works_mobile__' . $i . '@2x.png'); ?>" width="<?= $mobile_image_size[0]/2; ?>" height="<?= $mobile_image_size[1]/2; ?>" />
                            <?php 
                            } 
                        ?>

                        <div class="home__how-it-works__image-desktop">
                            <img src="<?= Assets\asset_path('images/how-it-works/desktop/how_it_works_desktop__' . $i . '.png'); ?>" />
                        </div>
                    </div>
                    <?php
                endwhile;
            endif;
            ?>

            <div class="home__how-it-works__spacer"></div>
        </div>

        <div class="home__how-it-works__button-container">
            <a class="home__how-it-works__button ui-button ui-button--primary" href="<?= $signup_url; ?>" style="background-color: <?= get_field('how_it_works_button_color'); ?>"><?= get_field('how_it_works_button_text'); ?></a>
        </div>

    </div>
</div>

<?php if (get_field('enable_prefooter')) { ?>
    <div class="home__prefooter home__section">
        <div class="container">
            <h2 class="home__section__heading"><?= get_field('prefooter_heading'); ?></h2>
            <h3 class="home__section__subheading"><?= get_field('prefooter_subheading'); ?></h3>

            <?php 
            if (have_rows('prefooter_sections')) :
                ?>
                <div class="home__prefooter__sections">
                    <?php
                    while (have_rows('prefooter_sections')) : the_row();
                        ?>
                        <div class="home__prefooter__section">
                            <img class="home__prefooter__section__image" src="<?= get_sub_field('image')['sizes']['large']; ?>" />

                            <div class="home__prefooter__section__heading"><?= get_sub_field('heading'); ?></div>
                            <div class="home__prefooter__section__heading-border"></div>

                            <div class="home__prefooter__section__copy"><?= get_sub_field('copy'); ?></div>
                        </div>
                        <?php
                    endwhile;
                ?>
                </div>
                <?php
            endif;
            ?>

            <div class="home__prefooter__button-container">
                <a class="home__prefooter__button ui-button ui-button--primary" href="<?= $signup_url; ?>" style="background-color: <?= get_field('prefooter_button_color'); ?>"><?= get_field('prefooter_button_text'); ?></a>
            </div>
        </div>
    </div>
<?php } ?>

<div class="home__footer-cta home__section" style="background-image: url(<?= get_field('footer_image')['sizes']['max']; ?>);">
    <div class="container">
        <div class="home__footer-cta__content">
            <h2 class="home__footer-cta__heading home__section__heading"><?= get_field('footer_heading'); ?></h2>

            <?php if (get_field('footer_subheading')) { ?>
                <h3 class="home__footer-cta__subheading home__section__subheading"><?= get_field('footer_subheading'); ?></h3>
            <?php } ?>

            <a class="home__cta__button ui-button ui-button--secondary" href="<?= $signup_url; ?>" style="background-color: <?= get_field('footer_button_color'); ?>"><?= get_field('footer_button_text'); ?></a>
        </div>
    </div>
</div>
