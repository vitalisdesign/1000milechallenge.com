<?php 
use Roots\Sage\Assets;
?>

<header class="header">
    <div class="header__inner-container container">
        <div class="header__logo-toggle-container">
            <a class="header__logo" href="<?= esc_url(home_url('/')); ?>">
                <img src="<?= Assets\asset_path('images/1000_mile_challenge_logo.svg'); ?>" />
            </a>

            <a class="header__toggle">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>

        <div class="header__nav-container">
            <nav class="header__nav">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(['theme_location' => 'primary_navigation', 'container' => '']);
                endif;
                ?>
            </nav>

            <div class="header__search">
                <?php echo file_get_contents(Assets\asset_path('images/search.svg')); ?>

                <form class="header__search-form">
                    <input type="search" id="search" name="search" placeholder="Enter phrase, email or name...">
                </form>
            </div>
        </div>
    </div>
</header>
