<?php
use Roots\Sage\Setup;
?>

<footer class="footer">
    <div class="container">
        <p>&copy; <?php printf('%s %s. %s', date('Y'), get_bloginfo('name'), __('All Rights Reserved')); ?>. <a href="<?= Setup\get_app_url('privacy-policy'); ?>">Privacy Policy</a></p>
    </div>
</footer>
