<div class="header__spacer"></div>

<div class="page-content">
    <div class="container">
        <?php get_template_part('templates/page', 'header'); ?>

        <div class="alert alert-warning">
            <?php _e('Sorry, but the page you were trying to view does not exist.', 'sage'); ?>
        </div>

        <br /><br />

        <a href="<?= esc_url(home_url('/')); ?>" class="ui-button ui-button--primary">Go Back to the Homepage</a>
    </div>
</div>
