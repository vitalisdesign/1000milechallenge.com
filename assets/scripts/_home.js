(function() {
    jQuery(document).ready(function() {
        /**
         * Initialize Slick Carousel for home slider.
         */
        jQuery('.home__community__testimonials').slick({
            arrows: false,
            dots: true,
            mobileFirst: true,
            speed: 350,
            responsive: [
                {
                    breakpoint: 992,
                    settings: 'unslick'
                }
            ]
        });
    });

    jQuery(window).resize(function() {
        jQuery('.home__community__testimonials').slick('resize');
    });

    /**
     * Initialize ScrollMagic.
     */
    function initScrollMagic() {
        var controller = new ScrollMagic.Controller();

        // configure panel nav
        controller.scrollTo(function(target) {
            var offset = jQuery(window).height() / 5.5;
            if (jQuery(window).width() >= 1440) {
                offset = jQuery(window).height() / 4.5;
            }

            target = target - offset;
            TweenMax.to(window, 0.5, {scrollTo: {y: target}});
        });

        var panelNav = '.home__how-it-works__nav';
        jQuery(panelNav + ' a').click(function(e) {
            var id = jQuery(this).attr('href');
            if (jQuery(id).length > 0) {
                e.preventDefault();
                controller.scrollTo(id);
            }
        });

        var panelNavDuration = jQuery('.home__how-it-works__list').height() - jQuery('.home__how-it-works__spacer').height() - 130;

        new ScrollMagic.Scene({
                triggerElement: panelNav,
                duration: panelNavDuration
            })
            .setClassToggle(panelNav, 'active')
            .setPin(panelNav)
            // .addIndicators()
            .addTo(controller);

        // configure panels
        var panels = jQuery('.home__how-it-works__item');
        jQuery('.home__how-it-works__item').each(function(index) {
            var panelIndex = index + 1;
            var currentPanel = '.home__how-it-works__item-' + panelIndex;
            var currentNavPanel = '#nav-panel-' + panelIndex;
            var duration = jQuery(currentPanel).height() + 120;

            if (panelIndex === panels.length) {
                duration = jQuery(currentPanel).height() - 10;
            }

            new ScrollMagic.Scene({
                    triggerElement: currentPanel,
                    duration: duration,
                    offset: 200
                })
                .setClassToggle(currentPanel  + ',' + currentNavPanel, 'active')
                .setPin(currentPanel + ' .home__how-it-works__image-desktop', {pushFollowers: false})
                // .addIndicators()
                .addTo(controller)
                .on('start', function(event) {
                    if (panelIndex === 1 && event.scrollDirection === 'REVERSE') {
                        jQuery('.home__how-it-works__item').first().addClass('active-fixed');
                    } else if (panelIndex === 1 && event.scrollDirection === 'FORWARD') {
                        jQuery('.home__how-it-works__item').first().removeClass('active-fixed');
                    }
                })
                .on('end', function(event) {
                    if (panelIndex === panels.length && event.scrollDirection === 'FORWARD') {
                        jQuery('.home__how-it-works__item').last().addClass('active-fixed');
                    } else if (panelIndex === panels.length && event.scrollDirection === 'REVERSE') {
                        jQuery('.home__how-it-works__item').last().removeClass('active-fixed');
                    }
                });
        });
    }

    jQuery(window).load(function() {
        // initialize ScrollMagic on desktop only
        if (jQuery(window).width() >= 992) {
            initScrollMagic();
        }
    });
})();
