/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

function initHeader() {
    jQuery('.header__toggle').click(function() {
        jQuery(this).toggleClass('header__toggle--open');
        jQuery('.header').toggleClass('header--open');
        jQuery('.content-fader').toggleClass('header-menu--open');
    });

    jQuery(document).on('click', function(event) {
        if (!jQuery(event.target).closest('.header').length) {
            jQuery('.header').removeClass('header--open');
            jQuery('.header__toggle').removeClass('header__toggle--open');
            jQuery('.content-fader').removeClass('header-menu--open');
        }

        if (!jQuery(event.target).closest('.header__search').length) {
            jQuery('.header__nav-container').removeClass('header__search--active');
        }
    });

    jQuery('.header__search svg').on('click', function() {
        jQuery(this).parent().parent().toggleClass('header__search--active');

        if (jQuery(this).parent().parent().hasClass('header__search--active')) {
            jQuery(this).parent().find('input[type=search]').focus();    
        } else {
            jQuery(this).parent().parent().find('input[type=search]').blur();
        }
    });

    jQuery('.header__search-form').on('submit', function() {
        window.location = ajax.appUrl + 'search/' + encodeURIComponent(jQuery(this).find('input[type=search]').val());
        return false;
    });
}

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                initHeader();
            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
